import torch
from dtaidistance import dtw_ndim
import numpy as np
from doctest import OutputChecker
import torch
import torchvision
import torch.optim as optim
import argparse
import matplotlib
import torch.nn as nn
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
import linearVae
from tqdm import tqdm
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision.utils import save_image
from Penn_actionDataset import Penn_actionDataset
from imageLoader import loadingImage
import torch
import os
from PIL import Image
import torchvision.transforms as transforms
import csv
import pandas as pd
import os
from vaeutil import findMinCost

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model =  linearVae.LinearVAE(input_f=49152, out_f=1680, features=300).to(device)
model.load_state_dict(torch.load("model.pth"))
model.eval()
currentVid = 0
debug = False
def createFolderName(num) -> str:
    total_len = 4
    numS = str(num)
    result=""
    for i in range(total_len-len(numS)):
        result = result + '0'
    result = result + numS
    return result    

def buildVidData(vidNum,model ,path,transformPip=None, amount=999999):
    current_vid = createFolderName(vidNum)
    path = os.path.join(path,current_vid)
    print("break point 0, the path: ",path)
    transform_t = transforms.Compose([
        transforms.Resize((128,128))
    ])
    library = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if debug:
                print("break point 2: ", file)
            with Image.open(os.path.join(root, file), "r") as img:
                if transformPip is not None:
                    img = transform_t(img)
                    img = transformPip(img) 
                library.append(img)
            
                amount -= 1
                # print("loading Image: ", img, "current amount count: ",amount)
                if(amount == 0):
                    break
    curData =  Penn_actionDataset(library)
    curDL = DataLoader(curData,shuffle=False)
    print("break point 1, curData", curData, )
    # it = iter(curDL)
    # print(it.next())
    result = []
    for data in curDL:
        data = data.to(device)
        f_l = torch.nn.Flatten()
        data = f_l(data)
        if debug:
            print("break point 4, load in data.shape: ", data.shape)
        result.append(model.encode(data).tolist())
    return result




transform = transforms.Compose([
    transforms.ToTensor(),
])
v1 = buildVidData(1,model,"Penn_Action\\frames",transformPip=transform)
v2 = buildVidData(2,model,"Penn_Action\\frames",transformPip=transform)
v1_split = v1[0:30]
v2_split = v2[0:30]
if debug:
    print("v1 length: ", len(v1), "v2 length: ", len(v2))
v1t = torch.tensor(v1_split)
v1t = torch.squeeze(v1t,dim=1)
v2t = torch.tensor(v2_split)
v2t = torch.squeeze(v2t,dim=1)

distance, paths = dtw_ndim.warping_paths(v1t.numpy(), v2t.numpy())
print("Here is path: ", type(paths))
paths = paths[1:,1:]
patht = []
path = findMinCost(paths.tolist(), patht)
print(path[1])

