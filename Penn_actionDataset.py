import torch
from torch.utils.data import Dataset
import pandas as pd
import os
import torchvision.transforms as transforms
from PIL import Image
debug = False
class Penn_actionDataset(Dataset):
    def __init__(self, data , transform=None):
        self.transform = transform
        self.data = data
    

    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, idx):
        row = self.data[idx]
        if self.transform is not None:
            row = self.transform(row)
        return row
        


