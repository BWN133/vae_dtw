import torch
import torch.nn as nn
import torch.nn.functional as F

# define a simple linear VAE
class LinearVAE(nn.Module):
    def __init__(self, input_f=784, out_f=512, features = 16):
        super(LinearVAE, self).__init__()
        self.features = features
        # encoder
        self.enc1 = nn.Linear(in_features=input_f, out_features=out_f)
        self.enc2 = nn.Linear(in_features=out_f, out_features=features*2)
 
        # decoder 
        self.dec1 = nn.Linear(in_features=features, out_features=out_f)
        self.dec2 = nn.Linear(in_features=out_f, out_features=input_f)
    def reparameterize(self, mu, log_var):
        """
        :param mu: mean from the encoder's latent space
        :param log_var: log variance from the encoder's latent space
        """
        std = torch.exp(0.5*log_var) # standard deviation
        eps = torch.randn_like(std) # `randn_like` as we need the same size
        sample = mu + (eps * std) # sampling as if coming from the input space
        return sample
 
    def forward(self, x):
        # encoding
        x = F.relu(self.enc1(x))
        x = self.enc2(x).view(-1, 2, self.features)
        # get `mu` and `log_var`
        mu = x[:, 0, :] # the first feature values as mean
        log_var = x[:, 1, :] # the other feature values as variance
        # get the latent vector through reparameterization
        z = self.reparameterize(mu, log_var)
 
        # decoding
        x = F.relu(self.dec1(z))
        reconstruction = torch.sigmoid(self.dec2(x))
        return reconstruction, mu, log_var
    
    def encode(self, x):
        x = F.relu(self.enc1(x))
        x = self.enc2(x).view(-1, 2, self.features)
        # get `mu` and `log_var`
        mu = x[:, 0, :] # the first feature values as mean
        log_var = x[:, 1, :] # the other feature values as variance
        # get the latent vector through reparameterization
        z = self.reparameterize(mu, log_var)
        return z
    def decode(self, z):
        x = F.relu(self.dec1(z))
        reconstruction = torch.sigmoid(self.dec2(x))
        return reconstruction