import torch
import torch.nn as nn
import torch.nn.functional as F

# define a simple linear VAE
debug = True
class deepNetVae_pen(nn.Module):
    def __init__(self,hidden_dims=None, in_channels=3, latent_dim=128):
        super(deepNetVae_pen, self).__init__()
        modules = []
        if hidden_dims is None:
            hidden_dims = [32, 64, 128, 256, 512]
        # self.features = features
        for h_dim in hidden_dims:
            modules.append(
                nn.Sequential(
                    nn.Conv2d(in_channels, out_channels=h_dim,
                              kernel_size= 3, stride= 2, padding  = 1),
                    nn.BatchNorm2d(h_dim),
                    nn.LeakyReLU())
            )
            in_channels = h_dim

        self.encoder = nn.Sequential(*modules)
        self.fc_mu = nn.Linear(hidden_dims[-1]*16, latent_dim)
        self.fc_var = nn.Linear(hidden_dims[-1]*16, latent_dim)


        # Build Decoder
        modules = []

        self.decoder_input = nn.Linear(latent_dim, hidden_dims[-1] * 4)

        hidden_dims.reverse()

        for i in range(len(hidden_dims) - 1):
            print("current i is there: ", hidden_dims[i])
            modules.append(
                nn.Sequential(
                    nn.ConvTranspose2d(hidden_dims[i],
                                       hidden_dims[i + 1],
                                       kernel_size=3,
                                       stride = 4,
                                       padding=1,
                                       output_padding=1),
                    nn.BatchNorm2d(hidden_dims[i + 1]),
                    nn.LeakyReLU())
            )



        self.decoder = nn.Sequential(*modules)

        self.final_layer = nn.Sequential(
                            nn.ConvTranspose2d(hidden_dims[-1],
                                               hidden_dims[-1],
                                               kernel_size=3,
                                               stride=1,
                                               padding=1,
                                               output_padding=0),
                            nn.BatchNorm2d(hidden_dims[-1]),
                            nn.LeakyReLU(),
                            nn.Conv2d(hidden_dims[-1], out_channels= 3,
                                      kernel_size= 3, padding= 1),
                            nn.Tanh())
        # # encoder
        # self.enc1 = nn.Linear(in_features=input_f, out_features=out_f)
        # self.enc2 = nn.Linear(in_features=out_f, out_features=features*2)
 
        # # decoder 
        # self.dec1 = nn.Linear(in_features=features, out_features=out_f)
        # self.dec2 = nn.Linear(in_features=out_f, out_features=input_f)
    def encode(self, input: torch.Tensor) -> list[torch.Tensor]:
        """
        Encodes the input by passing through the encoder network
        and returns the latent codes.
        :param input: (Tensor) Input tensor to encoder [N x C x H x W]
        :return: (Tensor) List of latent codes
        """
        result = self.encoder(input)
        if debug:
            print("break point 1 in deepNetVae.encode()!!!!!!!!!!!!!!!!! input.shape:", input.shape)
            print("break point 1 in deepNetVae.encode()!!!!!!!!!!!!!!!!! result.shape:", result.shape)
        result = torch.flatten(result, start_dim=1)
        if debug:
            print("break point 2 in deepNetVae.encode()!!!!!!!!!!!!!!!!! result.shape:", result.shape)

        # Split the result into mu and var components
        # of the latent Gaussian distribution
        mu = self.fc_mu(result)
        log_var = self.fc_var(result)

        return [mu, log_var]

    def decode(self, z):
        """
        Maps the given latent codes
        onto the image space.
        :param z: (Tensor) [B x D]
        :return: (Tensor) [B x C x H x W]
        """
        result = self.decoder_input(z)
        if debug:
            print("break point 6, result after decoder_onput: ", result.shape)
        result = result.view(-1, 512, 2, 2)
        if debug:
            print("break point 7, after result.view: ", result.shape)
        result = self.decoder(result)
        if debug:
            print("first decoded result: ", result.shape)
        result = self.final_layer(result)
        if debug:
            print("after final layer result: ", result.shape)
        return result

    def reparameterize(self, mu, logvar):
        """
        Reparameterization trick to sample from N(mu, var) from
        N(0,1).
        :param mu: (Tensor) Mean of the latent Gaussian [B x D]
        :param logvar: (Tensor) Standard deviation of the latent Gaussian [B x D]
        :return: (Tensor) [B x D]
        """
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return eps * std + mu

    # def reparameterize(self, mu, log_var):
    #     """
    #     :param mu: mean from the encoder's latent space
    #     :param log_var: log variance from the encoder's latent space
    #     """
    #     std = torch.exp(0.5*log_var) # standard deviation
    #     eps = torch.randn_like(std) # `randn_like` as we need the same size
    #     sample = mu + (eps * std) # sampling as if coming from the input space
    #     return sample
 
    # def forward(self, x):
    #     # encoding
    #     x = F.relu(self.enc1(x))
    #     x = self.enc2(x).view(-1, 2, self.features)
    #     # get `mu` and `log_var`
    #     mu = x[:, 0, :] # the first feature values as mean
    #     log_var = x[:, 1, :] # the other feature values as variance
    #     # get the latent vector through reparameterization
    #     z = self.reparameterize(mu, log_var)
 
    #     # decoding
    #     x = F.relu(self.dec1(z))
    #     reconstruction = torch.sigmoid(self.dec2(x))
    #     return reconstruction, mu, log_var

    def forward(self, input, **kwargs):
        mu, log_var = self.encode(input)
        z = self.reparameterize(mu, log_var)
        if debug:
            print("break point 5, z ere!!!!!!!!!!!!!!!!!!!!!!!!!: ", z.shape)
        return  [self.decode(z), input, mu, log_var]

    # def loss_function(self,
    #                   *args,
    #                   **kwargs):

    def loss_function(self,
                      *args):
        """
        Computes the VAE loss function.
        KL(N(\mu, \sigma), N(0, 1)) = \log \frac{1}{\sigma} + \frac{\sigma^2 + \mu^2}{2} - \frac{1}{2}
        :param args:
        :param kwargs:
        :return:
        """
        recons = args[0]
        input = args[1]
        mu = args[2]
        log_var = args[3]

        # kld_weight = kwargs['M_N'] # Account for the minibatch samples from the dataset
        if debug:
            print("break point 4, recons: ", recons.shape)
            print("break point 4, input: ", input.shape)
        recons_loss =F.mse_loss(recons, input)


        kld_loss = torch.mean(-0.5 * torch.sum(1 + log_var - mu ** 2 - log_var.exp(), dim = 1), dim = 0)

        # loss = recons_loss + kld_weight * kld_loss
        loss = recons_loss +  0.00025*kld_loss
        return {'loss': loss, 'Reconstruction_Loss':recons_loss.detach(), 'KLD':-kld_loss.detach()}