import torch
import os
from PIL import Image
import torchvision.transforms as transforms
import csv
import pandas as pd

# Set of magic numbers: 
INFINITE_N=999999999
def loadingImage(path, amount=INFINITE_N, transformPip = None, image_resize=128):
    library = []

    transform_t = transforms.Compose([
        transforms.Resize((image_resize,image_resize))
    ])
    for root, dirs, files in os.walk(path):
        for file in files:
            with Image.open(os.path.join(root, file), "r") as img:
                if transformPip is not None:
                    img = transform_t(img)
                    img = transformPip(img) 
                library.append(img)
                
                amount -= 1
                if(amount == 0):
                    return library
    return library


def averageShape(bank):
    totalWidth = 0
    totalHeight = 0
    for i in bank:
        curS = torch.tensor(bank[0]).shape
        totalWidth += curS[1]
        totalHeight += curS[2]
    return totalWidth/len(bank), totalHeight/len(bank)
