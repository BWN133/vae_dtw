from itertools import tee
import sys
 
 
# Naive recursive function to find the minimum cost to reach
# cell (m, n) from cell (0, 0)

def findMinCost(cost, path ,m=None, n=None):
    # initialize m and n
    if not m and not n:
        m, n = len(cost), len(cost[0])
 
    # base case
    if not cost or not len(cost):
        return (0, path)
 
    # base case
    if n == 0 or m == 0:
        return (sys.maxsize, path)
 
    # if we are in the first cell (0, 0)
    if m == 1 and n == 1:
        npath = path.copy()
        npath.append((1,1))
        return (cost[0][0], npath)
 
    # include the current cell's cost in the path and recur to find the minimum
    # of the path from the adjacent left cell and adjacent top cell.

    npath = path.copy()
    npath.append((m,n))
    td = findMinCost(cost,npath ,m - 1, n)
    costDown = td[0]
    pathDown = td[1]
    tr = findMinCost(cost, npath,m, n - 1)
    costRight = tr[0]
    pathRight = tr[1]
    if costDown <= costRight:
        return (costDown + cost[m - 1][n - 1], pathDown)
    else:
        return (costRight + cost[m-1][n-1], pathRight)
    # return (min(findMinCost(cost,npath ,m - 1, n)[0], findMinCost(cost, npath,m, n - 1)[0])\
    #     + cost[m - 1][n - 1], npath)
 
 
# if __name__ == '__main__':
 
#     cost = [
#         [4, 7, 8, 6, 4],
#         [6, 7, 3, 9, 2],
#         [3, 8, 1, 2, 4],
#         [7, 1, 7, 3, 7],
#         [2, 9, 8, 9, 3]
#     ]
# path = []
# p = findMinCost(cost, path)

# print(p[1])