from doctest import OutputChecker
import torch
import torchvision
import torch.optim as optim
import argparse
import matplotlib
import torch.nn as nn
import matplotlib.pyplot as plt
import torchvision.transforms as transforms

from tqdm import tqdm
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision.utils import save_image
from deepNetVae_penn import deepNetVae_pen
from Penn_actionDataset import Penn_actionDataset

from imageLoader import loadingImage
matplotlib.style.use('ggplot')
parser = argparse.ArgumentParser()
parser.add_argument('-e', '--epochs', default=100, type=int, 
                    help='number of epochs to train the VAE for')
args = vars(parser.parse_args())
# leanring parameters
epochs = args['epochs']
batch_size = 32

lr = 0.005
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
transform = transforms.Compose([
    transforms.ToTensor(),
])
data_bank = loadingImage("Penn_Action/frames",amount=100,transformPip=transform)
train_data = Penn_actionDataset(data_bank)
val_data = Penn_actionDataset(data_bank)

# training and validation data loaders
train_loader = DataLoader(
    train_data,
    batch_size=batch_size,
    shuffle=True
)
val_loader = DataLoader(
    val_data,
    batch_size=batch_size,
    shuffle=False
)

model = deepNetVae_pen().to(device)
optimizer = optim.Adam(model.parameters(), lr=lr)
# criterion = nn.BCELoss(reduction='sum')
# def final_loss(bce_loss, mu, logvar):
#     """
#     This function will add the reconstruction loss (BCELoss) and the 
#     KL-Divergence.
#     KL-Divergence = 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
#     :param bce_loss: recontruction loss
#     :param mu: the mean from the latent vector
#     :param logvar: log variance from the latent vector
#     """
#     BCE = bce_loss 
#     KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
#     return BCE + KLD

def fit(model, dataloader):
    model.train()
    running_loss = 0.0
    for i, data in tqdm(enumerate(dataloader), total=int(len(train_data)/dataloader.batch_size)):
        # print("break point 1, data.size", data.shape)
        data = data.to(device)
        # data = data.view(data.size(0), -1)
        optimizer.zero_grad()
        results = model(data)
        loss = model.loss_function(*results)
        # print("break point 3, reconstrcution shape!!!!!!!: ", reconstruction.shape)
        # bce_loss = criterion(reconstruction, data)
        # print("break point 2, bce_loss (recon loss) ", bce_loss)
        # loss = final_loss(bce_loss, mu, logvar)
        # running_loss += loss.item()
        running_loss += loss['loss']
        loss['loss'].backward()
        optimizer.step()
    train_loss = running_loss/len(dataloader.dataset)
    return train_loss

# def validate(model, dataloader):
#     model.eval()
#     running_loss = 0.0
#     with torch.no_grad():
#         for i, data in tqdm(enumerate(dataloader), total=int(len(val_data)/dataloader.batch_size)):
#             data, _ = data
#             data = data.to(device)
#             data = data.view(data.size(0), -1)
#             reconstruction, mu, logvar = model(data)
#             bce_loss = criterion(reconstruction, data)
#             loss = final_loss(bce_loss, mu, logvar)
#             running_loss += loss.item()
        
#             # save the last batch input and output of every epoch
#             if i == int(len(val_data)/dataloader.batch_size) - 1:
#                 num_rows = 8
#                 both = torch.cat((data.view(batch_size, 1, 28, 28)[:8], 
#                                   reconstruction.view(batch_size, 1, 28, 28)[:8]))
#                 save_image(both.cpu(), f"../outputs/output{epoch}.png", nrow=num_rows)
#     val_loss = running_loss/len(dataloader.dataset)
#     return val_loss
def compare_pic(dataloader, model):
    with torch.no_grad():
        for i, data in tqdm(enumerate(dataloader), total=int(len(val_data)/dataloader.batch_size)):
            data = data.to(device)
            # data = data.view(data.size(0), -1)
            results= model(data)
            if i == int(len(val_data)/dataloader.batch_size) - 1:
                num_rows = 8
                both = torch.cat((data[:8], 
                                  results[0][:8]))
                save_image(both.cpu(), f"outputs/output{epoch}.png", nrow=num_rows)
        
            
train_loss = []
# val_loss = []
for epoch in range(epochs):
    print(f"Epoch {epoch+1} of {epochs}")
    train_epoch_loss = fit(model, train_loader)
    # val_epoch_loss = validate(model, val_loader)
    train_loss.append(train_epoch_loss)
    # val_loss.append(val_epoch_loss)
    print(f"Train Loss: {train_epoch_loss:.4f}")
    compare_pic(train_loader, model)
    
    # print(f"Val Loss: {val_epoch_loss:.4f}")