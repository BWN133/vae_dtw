from doctest import OutputChecker
import torch
import torchvision
import torch.optim as optim
import argparse
import matplotlib
import torch.nn as nn
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
import linearVae
from tqdm import tqdm
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision.utils import save_image
from Penn_actionDataset import Penn_actionDataset
from imageLoader import loadingImage
from sklearn.model_selection import train_test_split
matplotlib.style.use('ggplot')
parser = argparse.ArgumentParser()
parser.add_argument('-e', '--epochs', default=100, type=int, 
                    help='number of epochs to train the VAE for')
parser.add_argument('-d','--dataPath', default="Penn_Action/frames", help="path to target data")
parser.add_argument('-a,', '--amount', default=2000, help="amount of data to read in the dataset")
parser.add_argument('-m', '--model', default='Linear', help="choose model to run: Linear, Deep")
parser.add_argument('-b','--batchSize', default=64, help='specify batch size')
args = vars(parser.parse_args())
# leanring parameters
epochs = args['epochs']
batch_size = args['batchSize']
image_resize = 128
lr = 0.0005
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
transform = transforms.Compose([
    transforms.ToTensor(),
])
data_bank = loadingImage(args['dataPath'],amount=args['amount'],transformPip=transform,image_resize=image_resize)

train_data = Penn_actionDataset(data_bank[0:1499])
val_data = Penn_actionDataset(data_bank[1500:1999])

# training and validation data loaders
train_loader = DataLoader(
    train_data,
    batch_size=batch_size,
    shuffle=True
)
val_loader = DataLoader(
    val_data,
    batch_size=batch_size,
    shuffle=False
)
# print(train_data)

model = linearVae.LinearVAE(input_f=3*image_resize*image_resize, out_f=1680, features=300).to(device)
optimizer = optim.Adam(model.parameters(), lr=lr)
criterion = nn.BCELoss(reduction='sum')
def final_loss(bce_loss, mu, logvar):
    """
    This function will add the reconstruction loss (BCELoss) and the 
    KL-Divergence.
    KL-Divergence = 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    :param bce_loss: recontruction loss
    :param mu: the mean from the latent vector
    :param logvar: log variance from the latent vector
    """
    BCE = bce_loss 
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
    return BCE + KLD

def fit(model, dataloader):
    model.train()
    running_loss = 0.0
    for i, data in tqdm(enumerate(dataloader), total=int(len(train_data)/dataloader.batch_size)):
        data = data.to(device)
        data = data.view(data.size(0), -1)
        optimizer.zero_grad()
        reconstruction, mu, logvar = model(data)
        bce_loss = criterion(reconstruction, data)
        loss = final_loss(bce_loss, mu, logvar)
        running_loss += loss.item()
        loss.backward()
        optimizer.step()
    train_loss = running_loss/len(dataloader.dataset)
    return train_loss

def compare_pic(dataloader, model):
    model.eval()
    running_loss = 0.0
    with torch.no_grad():
        for i, data in tqdm(enumerate(dataloader), total=int(len(val_data)/dataloader.batch_size)):
            data = data.to(device)
            data = data.view(data.size(0), -1)
            reconstruction, mu, logvar = model(data)
            bce_loss = criterion(reconstruction, data)
            loss = final_loss(bce_loss, mu, logvar)
            running_loss += loss.item()
            if i == int(len(val_data)/dataloader.batch_size) - 1:
                num_rows = 8
                both = torch.cat((data.view(batch_size, 3, 128, 128)[:8], 
                                  reconstruction.view(batch_size, 3, 128, 128)[:8]))
                save_image(both.cpu(), f"outputs/output{epoch}.png", nrow=num_rows)
    val_loss = running_loss/len(dataloader.dataset)
    return val_loss
            
train_loss = []
val_loss = []
for epoch in range(epochs):
    print(f"Epoch {epoch+1} of {epochs}")
    train_epoch_loss = fit(model, train_loader)
    train_loss.append(train_epoch_loss)
    print(f"Train Loss: {train_epoch_loss:.4f}")
    val_loss_E = compare_pic(val_loader, model)
    val_loss.append(val_loss_E)
    print(f"Val Loss: {val_loss_E:.4f}")
torch.save(model.state_dict(), 'model.pth')